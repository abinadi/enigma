from exceptions.MultipleLettersPluginException import MultipleLettersPluginException
from exceptions.NonLetterPluginException import NonLetterPluginException
from exceptions.TooManyPlugsPluginException import TooManyPlugsPluginException


class Plugboard:
    plugs = 0

    board = {
        "a": "a",
        "b": "b",
        "c": "c",
        "d": "d",
        "e": "e",
        "f": "f",
        "g": "g",
        "h": "h",
        "i": "i",
        "j": "j",
        "k": "k",
        "l": "l",
        "m": "m",
        "n": "n",
        "o": "o",
        "p": "p",
        "q": "q",
        "r": "r",
        "s": "s",
        "t": "t",
        "u": "u",
        "v": "v",
        "w": "w",
        "x": "x",
        "y": "y",
        "z": "z"
    }

    def extraction(self, letter):
        self.validate_letters([letter])
        return self.board[letter.lower()]

    def plug(self, letter1, letter2):
        self.validate_letters([letter1, letter2])

        if self.plugs == 10:
            raise TooManyPlugsPluginException

        self.board[letter1.lower()] = letter2.lower()
        self.board[letter2.lower()] = letter1.lower()

        self.plugs = self.plugs + 1

    def unplug(self, letter):
        self.validate_letters([letter])

        letter = letter.lower()
        if self.board[letter] == letter:
            return

        letter2 = self.board[letter]

        self.board[letter] = letter
        self.board[letter2] = letter2

        self.plugs = self.plugs - 1

    def plug_count(self):
        return self.plugs

    def reset(self):
        for c in self.range_char("a", "z"):
            self.board[c] = c

        self.plugs = 0

    @staticmethod
    def validate_letters(letters):
        for letter in letters:
            if not isinstance(letter, str) or not letter.isalpha():
                raise NonLetterPluginException

            if len(letter) != 1:
                raise MultipleLettersPluginException

    @staticmethod
    def range_char(start, stop):
        return (chr(n) for n in range(ord(start), ord(stop) + 1))
