from exceptions.MultipleLettersReflectorException import MultipleLettersReflectorException
from exceptions.NonLetterReflectorException import NonLetterReflectorException


class Reflector:
    # The reflector is an alphabet that is mapped to itself. So matching
    # letters are reciprocal. For example, if "A" matches to "X", then
    # "X" will also match to "A"
    letter_pairs = {
        "a": "q",
        "b": "w",
        "c": "e",
        "d": "r",
        "e": "c",
        "f": "t",
        "g": "y",
        "h": "u",
        "i": "o",
        "j": "p",
        "k": "s",
        "l": "z",
        "m": "x",
        "n": "v",
        "o": "i",
        "p": "j",
        "q": "a",
        "r": "d",
        "s": "k",
        "t": "f",
        "u": "h",
        "v": "n",
        "w": "b",
        "x": "m",
        "y": "g",
        "z": "l"
    }

    @staticmethod
    def reflect(letter: str):
        if not isinstance(letter, str) or not letter.isalpha():
            raise NonLetterReflectorException

        if len(letter) != 1:
            raise MultipleLettersReflectorException

        return Reflector.letter_pairs[letter.lower()]
