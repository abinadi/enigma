import pytest

from Alphabet import Alphabet
from Rotor import Rotor
from RotorBox import RotorBox
from RotorBoxBuilder import RotorBoxBuilder
from exceptions.InvalidParameterRotorBoxBuilderException import InvalidParameterRotorBoxBuilderException
from exceptions.MissingParameterRotorBoxBuilderException import MissingParameterRotorBoxBuilderException


def test_can_build_a_rotorbox():
    builder = RotorBoxBuilder()
    builder.add_left_rotor(Rotor(Alphabet.mapping1, 10, "R"))
    builder.add_middle_rotor(Rotor(Alphabet.mapping2, 3, "F"))
    builder.add_right_rotor(Rotor(Alphabet.mapping3, 17, "W"))

    assert isinstance(builder.build(), RotorBox)


def test_build_will_fail_if_not_all_params_are_met():
    builder = RotorBoxBuilder()

    with pytest.raises(MissingParameterRotorBoxBuilderException):
        builder.build()


def test_can_only_build_with_rotors():
    builder = RotorBoxBuilder()

    with pytest.raises(InvalidParameterRotorBoxBuilderException):
        builder.add_left_rotor("Hello")

    with pytest.raises(InvalidParameterRotorBoxBuilderException):
        builder.add_middle_rotor(44)

    with pytest.raises(InvalidParameterRotorBoxBuilderException):
        builder.add_right_rotor(Alphabet.mapping1)
