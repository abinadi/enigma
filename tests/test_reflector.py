import pytest

from Reflector import Reflector
from exceptions.MultipleLettersReflectorException import MultipleLettersReflectorException
from exceptions.NonLetterReflectorException import NonLetterReflectorException


def test_reflector_will_reflect_a_pair_of_letters():
    assert "q" == Reflector.reflect("a")
    assert "a" == Reflector.reflect("q")


def test_reflector_can_accept_uppercase():
    assert "q" == Reflector.reflect("A")


def test_reflector_will_only_accept_single_letters():
    with pytest.raises(MultipleLettersReflectorException):
        Reflector.reflect("ABC")

    with pytest.raises(NonLetterReflectorException):
        Reflector.reflect(4)

    with pytest.raises(NonLetterReflectorException):
        Reflector.reflect("$")
