
import unittest
from Plugboard import Plugboard
from Rotor import Rotor
from Alphabet import Alphabet
from Coder import Coder
from RotorBoxBuilder import RotorBoxBuilder


class CoderTest(unittest.TestCase):
    def test_can_take_a_letter_and_give_a_letter(self):
        rotorbox = self._build_rotorbox()

        plugboard = Plugboard()
    
        coder = Coder(rotorbox, plugboard)
        
        code = coder.encode_letter("a")
        
        self.assertIsNotNone(code)
        self.assertIsInstance(code, str)
        self.assertNotEqual(code, "a")

    def test_can_take_multiple_letters_and_encode_them(self):
        rotorbox = self._build_rotorbox()
        plugboard = Plugboard()

        coder = Coder(rotorbox, plugboard)

        code = coder.encode("Abinadi")

        self.assertIsNotNone(code)
        self.assertIsInstance(code, str)
        self.assertNotEqual(code, "Abinadi")

    def test_can_encode_a_phrase_with_spaces(self):
        rotorbox = self._build_rotorbox()
        plugboard = Plugboard()

        coder = Coder(rotorbox, plugboard)

        code = coder.encode("I am sam")

        self.assertIsNotNone(code)
        self.assertIsInstance(code, str)
        self.assertNotEqual(code, "I am sam!")

    def test_can_decode_a_phrase(self):
        rotorbox = self._build_rotorbox()
        plugboard = Plugboard()

        coder = Coder(rotorbox, plugboard)

        code = coder.decode("zuuotwx")

        self.assertIsNotNone(code)
        self.assertIsInstance(code, str)
        self.assertEqual(code, "Abinadi".lower())

    @staticmethod
    def _build_rotorbox():
        builder = RotorBoxBuilder()
        builder.add_left_rotor(Rotor(Alphabet.mapping1, 1, "g"))
        builder.add_middle_rotor(Rotor(Alphabet.mapping2, 12, "n"))
        builder.add_right_rotor(Rotor(Alphabet.mapping3, 20, "r"))
        return builder.build()
