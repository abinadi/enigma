import unittest

from Rotor import Rotor
from Alphabet import Alphabet
from exceptions.IntegerOutOfBoundsRotorException import IntegerOutOfBoundsRotorException
from exceptions.MultipleLettersRotorException import MultipleLettersRotorException
from exceptions.NonIntegerRotorException import NonIntegerRotorException
from exceptions.NonLetterRotorException import NonLetterRotorException


class RotorTest(unittest.TestCase):
    def test_rotor_can_accept_a_letter_and_return_a_different_letter(self):
        # Given
        rotor = Rotor(Alphabet.mapping1)

        # When
        response = rotor.input('W')  # Could be any letter

        # Then
        self.assertIsNotNone(response)          # is not empty
        self.assertEqual(len(response), 1)      # is only one character
        self.assertIsInstance(response, str)    # is a string
        self.assertRegex(response, '[a-zA-Z]')  # is a letter of the alphabet

    def test_rotor_can_accept_a_left_input(self):
        rotor = Rotor(Alphabet.mapping1)

        response = rotor.input("e")
        left_response = rotor.left_input("G")

        self.assertEqual("g", response)
        self.assertEqual("e", left_response)

    def test_left_input_can_only_accept_letter(self):
        rotor = Rotor(Alphabet.mapping1)

        with self.assertRaises(NonLetterRotorException):
            rotor.left_input(3)

    def test_rotor_can_step(self):
        # Given
        rotor = Rotor(Alphabet.mapping1)

        # When
        response1 = rotor.input('a')
        rotor.step()
        response2 = rotor.input('a')

        # Then
        self.assertEqual(response1, 'f')
        self.assertNotEqual(response1, response2)

    def test_rotor_can_step2(self):
        # Given
        rotor = Rotor(Alphabet.mapping2)

        # When
        response1 = rotor.input('a')
        rotor.step()
        response2 = rotor.input('a')

        # Then
        self.assertEqual(response1, 'q')
        self.assertNotEqual(response1, response2)

    def test_rotor_can_start_at_any_position(self):
        rotor = Rotor(Alphabet.mapping1, 3)

        response = rotor.input('a')

        self.assertEqual(response, 'p')

    def test_can_determine_if_you_hit_the_notch(self):
        rotor = Rotor(Alphabet.mapping1, 25)

        self.assertFalse(rotor.hit_notch())
        rotor.step()
        self.assertFalse(rotor.hit_notch())
        rotor.step()
        self.assertTrue(rotor.hit_notch())

    def test_input_can_only_accept_a_single_letter(self):
        rotor = Rotor(Alphabet.mapping3, 1)

        with self.assertRaises(NonLetterRotorException):
            rotor.input(3)

        with self.assertRaises(MultipleLettersRotorException):
            rotor.input("ABC")

        with self.assertRaises(NonLetterRotorException):
            rotor.input("$")

    def test_position_must_be_a_positive_int_between_1_and_26(self):
        rotor = Rotor(Alphabet.mapping2, 1)

        with self.assertRaises(IntegerOutOfBoundsRotorException):
            rotor.set_start_position(0)

        with self.assertRaises(IntegerOutOfBoundsRotorException):
            rotor.set_start_position(-3)

        with self.assertRaises(IntegerOutOfBoundsRotorException):
            rotor.set_start_position(27)

        with self.assertRaises(NonIntegerRotorException):
            rotor.set_start_position(1.2)

        with self.assertRaises(NonIntegerRotorException):
            rotor.set_start_position("H")

        with self.assertRaises(NonIntegerRotorException):
            rotor.set_start_position("ABC")

        with self.assertRaises(NonIntegerRotorException):
            Rotor(Alphabet.mapping1, "a")

        with self.assertRaises(NonIntegerRotorException):
            Rotor(Alphabet.mapping1, 3.14)

        with self.assertRaises(IntegerOutOfBoundsRotorException):
            Rotor(Alphabet.mapping1, 45)
