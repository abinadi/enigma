import pytest

from exceptions.MultipleLettersPluginException import MultipleLettersPluginException
from exceptions.NonLetterPluginException import NonLetterPluginException
from exceptions.TooManyPlugsPluginException import TooManyPlugsPluginException
from Plugboard import Plugboard


@pytest.fixture
def plugboard():
    return Plugboard()


def test_with_no_plugs_letters_map_to_themselves(plugboard):
    alphabet = plugboard.range_char("a", "z")

    for c in alphabet:
        output = plugboard.extraction(c)
        assert c, output


def test_can_connect_letters(plugboard):
    plugboard.plug("t", "h")
    assert "h", plugboard.extraction("t")
    assert "t", plugboard.extraction("h")


def test_can_only_have_up_to_ten_plugs(plugboard):
    # Given 10 plugs in the plugboard
    plugboard.plug("a", "b")
    plugboard.plug("c", "d")
    plugboard.plug("e", "f")
    plugboard.plug("g", "h")
    plugboard.plug("i", "j")
    plugboard.plug("k", "l")
    plugboard.plug("m", "n")
    plugboard.plug("o", "p")
    plugboard.plug("q", "r")
    plugboard.plug("s", "t")

    # When you try to add one more plug
    # Assert an exception should be raised
    with pytest.raises(TooManyPlugsPluginException):
        plugboard.plug("u", "v")


def test_can_unplug_a_plug(plugboard):
    plugboard.plug("a", "b")
    plugboard.plug("c", "d")

    plugboard.unplug("a")
    plugboard.unplug("d")

    assert "a" == plugboard.extraction("a")
    assert "b" == plugboard.extraction("b")
    assert "c" == plugboard.extraction("c")
    assert "d" == plugboard.extraction("d")


def test_can_get_the_number_of_plugs(plugboard):
    plugboard.plug("a", "m")
    plugboard.plug("c", "x")
    plugboard.plug("t", "e")

    assert 3 == plugboard.plug_count()

    plugboard.unplug("c")

    assert 2 == plugboard.plug_count()


def test_can_reset_the_plugboard(plugboard):
    original = plugboard

    plugboard.plug("a", "f")
    plugboard.plug("z", "x")

    assert 2 == plugboard.plug_count()
    assert "f" == plugboard.extraction("a")
    assert "x" == plugboard.extraction("z")

    plugboard.reset()

    assert 0 == plugboard.plug_count()
    assert "a" == plugboard.extraction("a")
    assert "f" == plugboard.extraction("f")
    assert "z" == plugboard.extraction("z")
    assert "x" == plugboard.extraction("x")
    assert "c" == plugboard.extraction("c")
    assert original == plugboard


def test_can_handle_capital_letters(plugboard):
    plugboard.plug("A", "C")

    assert 1 == plugboard.plug_count()

    assert "c" == plugboard.extraction("A")
    assert "a" == plugboard.extraction("C")

    plugboard.unplug("A")
    assert 0 == plugboard.plug_count()


def test_can_only_accept_single_letters(plugboard):
    with pytest.raises(NonLetterPluginException):
        plugboard.plug("A", 3)

    with pytest.raises(NonLetterPluginException):
        plugboard.plug(45, "L")

    with pytest.raises(NonLetterPluginException):
        plugboard.extraction(113)

    with pytest.raises(NonLetterPluginException):
        plugboard.unplug(-7)

    with pytest.raises(MultipleLettersPluginException):
        plugboard.plug("ABINADI", "S")

    with pytest.raises(NonLetterPluginException):
        plugboard.extraction("#")
