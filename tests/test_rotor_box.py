import unittest

from Reflector import Reflector
from Rotor import Rotor
from Alphabet import Alphabet
from RotorBox import RotorBox
from exceptions.MultipleLettersRotorBoxException import MultipleLettersRotorBoxException
from exceptions.NonLetterRotorBoxException import NonLetterRotorBoxException


class RotorBoxTest(unittest.TestCase):
    def test_rotor_box_can_accept_a_letter_and_encrypt_it(self):
        # Given a new rotor box
        rotor1 = Rotor(Alphabet.mapping1, 1)
        rotor2 = Rotor(Alphabet.mapping2, 1)
        rotor3 = Rotor(Alphabet.mapping3, 1)
        reflector = Reflector()

        rotor_box = RotorBox(rotor1, rotor2, rotor3, reflector)

        # When you give it a letter
        response = rotor_box.encode("j")

        # Then You should get a letter back
        self.assertIsNotNone(response)
        self.assertIsInstance(response, str)

    def test_encode_can_only_accept_a_single_letter(self):
        rotor1 = Rotor(Alphabet.mapping1, 1)
        rotor2 = Rotor(Alphabet.mapping2, 1)
        rotor3 = Rotor(Alphabet.mapping3, 1)
        reflector = Reflector()

        rotor_box = RotorBox(rotor1, rotor2, rotor3, reflector)

        with self.assertRaises(NonLetterRotorBoxException):
            rotor_box.encode(3)

        with self.assertRaises(MultipleLettersRotorBoxException):
            rotor_box.encode("ABC")

        with self.assertRaises(NonLetterRotorBoxException):
            rotor_box.encode("$")
