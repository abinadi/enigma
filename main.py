
from Alphabet import Alphabet
from Coder import Coder
from Plugboard import Plugboard
from Rotor import Rotor
from RotorBoxBuilder import RotorBoxBuilder


def get_rotor(available):
    rotorstr = ', '.join(str(x) for x in available)

    # Get the rotor number
    if len(available) == 1:
        print("Rotor: " + str(available[0]))
        rotor = available[0]
    else:
        rotor = ask_question("Which rotor? [" + rotorstr + "] => ", available)

    # Get the position of the rotor
    position = ask_question("What position will it start at? [1-26] => ", range(1, 27))

    # Get the rotor's notch
    char_range = list(Plugboard.range_char('A', 'Z')) + list(Plugboard.range_char('a', 'z'))
    notch = ask_question("What letter is its notch at? [A-Z] => ", char_range, "string")

    return {
        "rotor": rotor,
        "position": position,
        "notch": notch
    }


def ask_question(question, choices, qtype="int"):
    while True:
        try:
            if qtype == "int":
                answer = int(input(question))
            else:
                answer = input(question)

            # Check if answer is valid
            if answer in choices:
                return answer
        except ValueError:
            print("Invalid choice (ValueError)")
            continue

        print("Invalid choice")


def get_alphabet(number):
    if number == 1:
        return Alphabet.mapping1
    elif number == 2:
        return Alphabet.mapping2
    elif number == 3:
        return Alphabet.mapping3
    else:
        print("Invalid alphabet choice")


def build_rotorbox():
    rotors = [1, 2, 3]

    left = get_rotor(rotors)
    rotors.remove(left["rotor"])

    print("\n")
    middle = get_rotor(rotors)
    rotors.remove(middle["rotor"])

    print("\n")
    right = get_rotor(rotors)

    builder = RotorBoxBuilder()
    builder.add_left_rotor(Rotor(get_alphabet(left["rotor"]), left["position"], left["notch"]))
    builder.add_middle_rotor(Rotor(get_alphabet(middle["rotor"]), middle["position"], middle["notch"]))
    builder.add_right_rotor(Rotor(get_alphabet(right["rotor"]), right["position"], right["notch"]))

    return builder.build()


def plug_plugboard():
    board = Plugboard()
    plugs = 0
    add_another = True
    char_range = list(Plugboard.range_char('a', 'z'))

    while add_another and plugs < 9:
        answer = input("Add a plug to the plugboard? [Y/N]")
        if answer in ["Y", "y"]:
            first_letter = input("First letter of pair to connect: ")
            if first_letter.lower() not in char_range:
                break
            char_range.remove(first_letter.lower())

            second_letter = input("Second letter of pair to connect: ")
            if second_letter.lower() not in char_range:
                break
            char_range.remove(second_letter.lower())

            board.plug(first_letter, second_letter)
            plugs = plugs + 1
        else:
            add_another = False

    return board


if __name__ == '__main__':
    rotorbox = build_rotorbox()

    plugboard = plug_plugboard()

    coder = Coder(rotorbox, plugboard)

    input_phrase = ''

    print("------------------------------")
    while input_phrase != '999':
        input_phrase = input("What do you want to encode/decode? => ")
        if input_phrase != '999':
            output = coder.encode(input_phrase)

            print("Here is the result: \n")
            print(output)

    print("------------------------------")
    print("Goodbye!! \n\n")
