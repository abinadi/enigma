from Reflector import Reflector
from Rotor import Rotor
from RotorBox import RotorBox
from exceptions.InvalidParameterRotorBoxBuilderException import InvalidParameterRotorBoxBuilderException
from exceptions.MissingParameterRotorBoxBuilderException import MissingParameterRotorBoxBuilderException


class RotorBoxBuilder:
    def __init__(self):
        self.left_rotor = None
        self.middle_rotor = None
        self.right_rotor = None
        self.reflector = Reflector()

    def add_left_rotor(self, rotor: Rotor):
        self.validate_rotor(rotor)
        self.left_rotor = rotor

    def add_middle_rotor(self, rotor: Rotor):
        self.validate_rotor(rotor)
        self.middle_rotor = rotor

    def add_right_rotor(self, rotor: Rotor):
        self.validate_rotor(rotor)
        self.right_rotor = rotor

    def build(self):
        self.can_build()

        return RotorBox(self.left_rotor, self.middle_rotor, self.right_rotor, self.reflector)

    def can_build(self):
        if self.left_rotor is None or self.middle_rotor is None or self.right_rotor is None:
            raise MissingParameterRotorBoxBuilderException

    @staticmethod
    def validate_rotor(rotor):
        if not isinstance(rotor, Rotor):
            raise InvalidParameterRotorBoxBuilderException
