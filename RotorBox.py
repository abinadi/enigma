from Reflector import Reflector
from Rotor import Rotor
from exceptions.MultipleLettersRotorBoxException import MultipleLettersRotorBoxException
from exceptions.NonLetterRotorBoxException import NonLetterRotorBoxException


class RotorBox:
    def __init__(self, left_rotor: Rotor, middle_rotor: Rotor, right_rotor: Rotor, reflector: Reflector):
        self.left_rotor = left_rotor
        self.middle_rotor = middle_rotor
        self.right_rotor = right_rotor
        self.reflector = reflector

    def step_rotors(self):
        if self.middle_rotor.hit_notch():
            self.left_rotor.step()

        if self.right_rotor.hit_notch():
            self.middle_rotor.step()

        self.right_rotor.step()

    def encode(self, letter):
        self.letter_verification(letter)

        # Step 1: pass through right rotor
        letter = self.right_rotor.input(letter)

        # Step 2: pass through middle rotor
        letter = self.middle_rotor.input(letter)

        # Step 3: pass through left rotor
        letter = self.left_rotor.input(letter)

        # Step 4: pass through reflector
        letter = self.reflector.reflect(letter)

        # Step 5: pass back through left rotor
        letter = self.left_rotor.left_input(letter)

        # Step 6: pass back through middle rotor
        letter = self.middle_rotor.left_input(letter)

        # Step 7: pass back through right rotor
        letter = self.right_rotor.left_input(letter)

        return letter

    @staticmethod
    def letter_verification(letter):
        if not isinstance(letter, str) or not letter.isalpha():
            raise NonLetterRotorBoxException

        if len(letter) != 1:
            raise MultipleLettersRotorBoxException

