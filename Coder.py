from Plugboard import Plugboard
from RotorBox import RotorBox


class Coder:
    def __init__(self, rotorbox: RotorBox, plugboard: Plugboard) -> None:
        self.plugbord = plugboard
        self.rotorbox = rotorbox

    def encode_letter(self, letter):
        result = self.plugbord.extraction(letter)
        result = self.rotorbox.encode(result)   
        result = self.plugbord.extraction(result)
        return result

    def encode(self, phrase):
        result = ""

        for letter in phrase:
            if letter.isalpha():
                # 1. rotate rotor(s)
                self.rotorbox.step_rotors()

                # 2. encode the letter (plugboard, rotorbox, plugboard)
                result = result + self.encode_letter(letter)
            else:
                result = result + letter

        # 3. output
        return result

    def decode(self, phrase):
        return self.encode(phrase)
