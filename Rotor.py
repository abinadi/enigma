from exceptions.IntegerOutOfBoundsRotorException import IntegerOutOfBoundsRotorException
from exceptions.LetterNotFoundRotorException import LetterNotFoundRotorException
from exceptions.MultipleLettersRotorException import MultipleLettersRotorException
from exceptions.NonIntegerRotorException import NonIntegerRotorException
from exceptions.NonLetterRotorException import NonLetterRotorException


class Rotor:
    pointer = 1

    def __init__(self, alphabet, start_position=1, notch="a") -> None:
        self.letter_verification(notch)

        self.alphabet = alphabet
        self.set_start_position(start_position)
        self.notch = notch.lower()
        self.pointer = start_position # Must come AFTER set_start_position() is called

    def input(self, letter):
        self.letter_verification(letter)

        return self.alphabet[letter.lower()]

    def left_input(self, letter):
        self.letter_verification(letter)

        for key, value in self.alphabet.items():
            if letter.lower() == value:
                return key

        raise LetterNotFoundRotorException

    def set_start_position(self, position):
        self.position_verification(position)

        x = 0
        while x < position - 1:
            self.step()
            x = x + 1

    def step(self):
        temp_alpha = {}

        for key, value in self.alphabet.items():
            temp_alpha[key] = self.alphabet.get(chr(ord(key) + 1))

        temp_alpha['z'] = self.alphabet['a']

        self.alphabet = temp_alpha

        self.update_pointer()

    def update_pointer(self):
        self.pointer = self.pointer + 1
        if self.pointer > 26:
            self.pointer = 1

    def hit_notch(self):
        # Subtract 96 from ord(notch) to get a num between 1-26
        return ((ord(self.notch)-96) % 26) == (self.pointer % 26)

    @staticmethod
    def letter_verification(letter):
        if not isinstance(letter, str) or not letter.isalpha():
            raise NonLetterRotorException

        if len(letter) != 1:
            raise MultipleLettersRotorException

    @staticmethod
    def position_verification(position):
        if not isinstance(position, int):
            raise NonIntegerRotorException

        if position < 1 or position > 26:
            raise IntegerOutOfBoundsRotorException
